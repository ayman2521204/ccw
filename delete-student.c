
#include "main.h"
#include <stdio.h>
#include <string.h>

void deleteStudentRecord(Student *students[]) {
    char name[51];
    int i;
    printf("\n\nEnter the Student Name: ");
    scanf(" %[^\n]", name);
    fflush(stdin);
    
    for (i = 0; i < 100; i++) {
        if (students[i] != NULL && strcmp(students[i]->name, name) == 0) {
            students[i] = NULL;
            break;
        }
    }
    if (i == 100) {
        printf("\n\nStudent Record Not Found!\n\n");
        return;
    }
    else
        printf("\n\nStudent Record Deleted Successfully!\n\n");
}
