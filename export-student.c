#include "main.h"
#include <stdio.h>
#include <sys/stat.h>


void exportStudentRecords(Student *students[], int id) {

    const char *filename = "records.csv";
    struct stat file_info;
    FILE *csv_file = NULL;

    if (stat(filename, &file_info) == 0) {
        csv_file = fopen("records.csv", "a");
    }
    else {
        csv_file = fopen("records.csv", "a");
        fprintf(csv_file, "Name,Date of Birth,Registration Number,Program Code,Annual Tuition\n");
    }
    
    if (csv_file == NULL){
        printf("\nError opening file.\n");
        return;
    }


    for (int i = 0; i < id; i++)
    {
        fprintf(csv_file, "%s,%s,%s,%s,%.2f\n", students[i]->name, students[i]->dateOfBirth,
                students[i]->registrationNumber, students[i]->programCode,
                students[i]->annualTuition);
    }

    fclose(csv_file);

    printf("\n\nStudent Record Exported Successfully!\n\n");
}