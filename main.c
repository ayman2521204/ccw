#include <stdio.h>
#include <string.h>
#include "main.h"


int main(void) {
    id = 0;
    while (id <= 100) {
        char choice;
        Student * student = NULL;
        printf("1. Create Student Record.");
        printf("\n2. Read Student Record.");
        printf("\n3. Update Student Record.");
        printf("\n4. Delete Student Record.");
        printf("\n5. Search For Student Record.");
        printf("\n6. Sort Student Records.");
        printf("\n7. Export Student Records.");
        printf("\n8. Exit.");
        printf("\nEnter your choice(1-8): ");
        scanf("%c", &choice);
        fflush(stdin); 
        
        switch (choice) {
            case '1':
                student = createStudentRecord(id, students);
                if (student != NULL) id++;
                break;
            case '2':
                readStudentRecord(students);
                break;
            case '3':
                updateStudentRecord(students);
                break;
            case '4':
                deleteStudentRecord(students);
                break;
            case '5':
                searchStudentRecord(students, id);
                break;
            case '6':
                sortStudentRecord(students, id); // work on this fix the empty cells..
                break;
            case '7':
                exportStudentRecords(students, id);
                break;
            case '8':
                return 1;
            default:
                printf("\n\nThat response seemed invalid, please try again...\n\n");
        }
    }
    
    return 0;
}