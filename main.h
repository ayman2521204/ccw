#ifndef MAIN_H
#define MAIN_H

typedef struct Student{
    char name[51];
    char dateOfBirth[11];
    char registrationNumber[7];
    char programCode[5];
    float annualTuition;
    int id;
} Student;

Student *students[100];
int id;

//includes
#include <stdio.h>
#include <string.h> 
#include <stdlib.h>
// function prototypes
Student * createStudentRecord(int id, Student *students[]);
void updateStudentRecord(Student *students[]);
void readStudentRecord(Student *students[]);
void deleteStudentRecord(Student *students[]);
void searchStudentRecord(Student *students[], int id);
void sortStudentRecord(Student *students[], int id);
void exportStudentRecords(Student *students[] , int id);

#endif