#include "main.h"
#include <stdio.h>
#include <string.h>

void searchStudentRecord(Student *students[], int id) {
    char data_entry[51];
    char choice;
    printf("\n\nWould you like to search by Student Name or Registration Number? (N/R): ");
    scanf(" %c", &choice);
    fflush(stdin);
    
    switch (choice)
    {
    case 'R':
        printf("\n\nEnter the Student Registration Number: ");
        scanf(" %[^\n]", data_entry);
        fflush(stdin);

        for (int i = 0; i < id; i++)
        {
            if (students[i] != NULL && strcmp(students[i]->registrationNumber, data_entry) == 0)
            {
                printf("\n\nStudent Name: %s", students[i]->name);
                printf("\nStudent Date of Birth: %s", students[i]->dateOfBirth);
                printf("\nStudent Registration Number: %s", students[i]->registrationNumber);
                printf("\nStudent Program Code: %s", students[i]->programCode);
                printf("\nStudent Annual Tuition: %.2f\n\n", students[i]->annualTuition);
                printf("\n\nStudent Record Found Successfully!\n\n");
                return;
            }
        }
        printf("\n\nStudent Record Not Found!\n\n");
        return;

        case 'N':
            printf("\n\nEnter the Student Name: ");
            scanf(" %[^\n]", data_entry);
            fflush(stdin);

            for (int i = 0; i < id; i++)
            {
                if (students[i] != NULL && strcmp(students[i]->name, data_entry) == 0)
                {
                    printf("\n\nStudent Name: %s", students[i]->name);
                    printf("\nStudent Date of Birth: %s", students[i]->dateOfBirth);
                    printf("\nStudent Registration Number: %s", students[i]->registrationNumber);
                    printf("\nStudent Program Code: %s", students[i]->programCode);
                    printf("\nStudent Annual Tuition: %.2f\n\n", students[i]->annualTuition);
                    printf("\n\nStudent Record Found Successfully!\n\n");
                    return;
                }
            }
            printf("\n\nStudent Record Not Found!\n\n");
            return;


        default:
            printf("\n\nInvalid Entry!\n\n");
            return;
        }
        printf("\n\nStudent Record Not Found!\n\n");
}