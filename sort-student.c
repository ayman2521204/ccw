#include "main.h"
#include <stdio.h>
#include <string.h>

void sortStudentRecord(Student *students[], int id)
{
    char choice;
    char data_entry[51];
    char names[id][51];
    char dates[id][11];

    printf("\n\nWhat would you like to sort the students by, name or date of birth? (N/D): ");
    scanf(" %c", &choice);
    fflush(stdin);

    switch (choice)
    {
    case 'N':
        for (int i = 0; i < id; i++)
        {
            if (students[i] != NULL)
            {
                strcpy(names[i], students[i]->name);
            }
        }
        for (int i = 0; i < id; i++)
        {
            for (int j = i + 1; j < id; j++)
            {
                if (strcmp(names[i], names[j]) > 0)
                {
                    char temp[51];
                    strcpy(temp, names[i]);
                    strcpy(names[i], names[j]);
                    strcpy(names[j], temp);
                }
            }
        }
        printf("\n\n[ ");
        for (int i = 0; i < id; i++)
        {
            if (i < 99)
                printf(" %s, ", names[i]);
            else
                printf(" %s", names[i]);
        }
        printf(" ]\n\n");
        break;

    case 'D':
        for (int i = 0; i < id; i++)
        {
            if (students[i] != NULL)
            {
                strcpy(dates[i], students[i]->dateOfBirth);
            }
        }
        for (int i = 0; i < id; i++)
        {
            for (int j = i + 1; j < id; j++)
            {
                if (strcmp(dates[i], dates[j]) > 0)
                {
                    char temp[11];
                    strcpy(temp, dates[i]);
                    strcpy(dates[i], dates[j]);
                    strcpy(dates[j], temp);
                }
            }
        }
        printf("\n\n[ ");
        for (int i = 0; i < id; i++)
        {
            if (i < 99)
                printf(" %s, ", dates[i]);
            else
                printf(" %s", dates[i]);
        }
        printf(" ]\n\n");
        break;

    default:
        printf("\n\nInvalid Entry!\n\n");
        return;
    }
    printf("\n\nStudent Record Sorted Successfully!\n\n");
}