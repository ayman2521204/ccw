// Creator:  Sunday Emmanuel Hezekiah
#include "main.h"
#include <stdio.h>
#include <string.h>

void updateStudentRecord(Student *students[]) {
    char name[51];
    printf("\n\nEnter the Student Name: ");
    scanf(" %[^\n]", name);
    
    Student *student = NULL;
    for (int i = 0; i < 100; i++) {
        if (students[i] != NULL && strcmp(students[i]->name, name) == 0) {
            student = students[i];
            break;
        }
    }
    if (student != NULL) {
        printf("\n\nEnter Student Name: ");
        scanf("%s", student->name);
        fflush(stdin);
        printf("\nEnter Student Date of Birth: ");
        scanf("%s", student->dateOfBirth);
        fflush(stdin);
        printf("\nEnter Student Registration Number: ");
        scanf("%s", student->registrationNumber);
        fflush(stdin);
        printf("\nEnter Student Program Code: ");
        scanf("%s", student->programCode);
        fflush(stdin);
        printf("\nEnter Student Annual Tuition: ");
        scanf("%f", &student->annualTuition);
        fflush(stdin);
    }
    else {
        printf("\n\nStudent Record Not Found!\n\n");
        return;
    }
    
    printf("\n\nStudent Record Updated Successfully!\n\n");
}